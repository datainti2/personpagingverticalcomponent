import { Component } from '@angular/core';

@Component({
  selector: 'sample-component',
  template: `<app-person-paging-vertical-comp></app-person-paging-vertical-comp>`
})
export class SampleComponent {

  constructor() {
  }

}
