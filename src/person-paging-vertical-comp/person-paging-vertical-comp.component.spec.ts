import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonPagingVerticalCompComponent } from './person-paging-vertical-comp.component';

describe('PersonPagingVerticalCompComponent', () => {
  let component: PersonPagingVerticalCompComponent;
  let fixture: ComponentFixture<PersonPagingVerticalCompComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonPagingVerticalCompComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonPagingVerticalCompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
