import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-person-paging-vertical-comp',
  templateUrl: './person-paging-vertical-comp.component.html',
  styleUrls: ['./person-paging-vertical-comp.component.css']
})
export class PersonPagingVerticalCompComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public persons: Array<any> = [
    { "name" : "Donald Trump",
      "img":"../assets/img/noimg.png",
      "statement" : 1718191}, 
    {"name" : "Barack Obama",
      "img":"../assets/img/noimg.png",
      "statement" : 7881},
    {"name" : "Trump",
      "img":"../assets/img/noimg.png",
      "statement" : 5869},
    {"name" : "Najib Razak",
      "img":"../assets/img/noimg.png",
      "statement" : 5286},
    {"name" : "Theresa May",
      "img":"../assets/img/noimg.png",
      "statement" : 5217},
    {"name" : "King Jong Nam",
      "img":"../assets/img/noimg.png",
      "statement" : 4878}];

}