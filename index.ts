import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SampleComponent } from './src/sample.component';
import { SampleDirective } from './src/sample.directive';
import { SamplePipe } from './src/sample.pipe';
import { SampleService } from './src/sample.service';
import {PersonPagingVerticalCompComponent} from './src/person-paging-vertical-comp/person-paging-vertical-comp.component';

export * from './src/sample.component';
export * from './src/sample.directive';
export * from './src/sample.pipe';
export * from './src/sample.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    SampleComponent,
    SampleDirective,
    SamplePipe,
    PersonPagingVerticalCompComponent

  ],
  exports: [
    SampleComponent,
    SampleDirective,
    SamplePipe
  ]
})
export class PersonPagingVerticalModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: PersonPagingVerticalModule,
      providers: [SampleService]
    };
  }
}
